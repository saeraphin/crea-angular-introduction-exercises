import { Component } from '@angular/core';
import { TodoService } from '../todo.service';
import mix from 'src/app/helpers/colors-mix';

const COLOR_BG_0 = '#f5f5f5'
const COLOR_BG_1 = '#00d1b2'
const COLOR_FG_0 = '#4a4a4a'
const COLOR_FG_1 = '#ffffff'

@Component({
  selector: 'app-todo-counter',
  templateUrl: './todo-counter.component.html',
  styleUrls: ['./todo-counter.component.css'],
  host: { class: 'tags has-addons' }
})
export class TodoCounterComponent {

  constructor(private service: TodoService) { }

  get isEmpty() {
    return this.service.items.length === 0
  }

  get doneCount() {
    return this.service.finishedItems.length
  }

  get todoCount() {
    return this.service.items.length
  }

  get style() {
    const ratio = this.doneCount / this.todoCount
    return {
      'background-color': mix(COLOR_BG_0, COLOR_BG_1, ratio),
      'color': ratio <= 0.5 ? COLOR_FG_0 : COLOR_FG_1,
    }
  }

}
