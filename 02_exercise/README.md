# Angular - exercice 2

Cet exercice continuera à se faire dans le module `todo`.

## 1. Créer un composant formulaire

![Angular Todo form screenshot](../00_todo-list-start/src/assets/images/todo-form.png?raw=true "Angular Todo list - form")

* Créer un composant `todo-form`\
  *Exécuter la commande depuis le répertoire `00_todo-list-start`*
  ```
  ng generate component todo/todo-form
  ```

* Rajouter le composant `todo-form` dans la vue du composant `todo` (Remplacer le bloc `Item form`)\
  `00_todo-list-start\src\app\todo\todo\todo.component.html`
* Rajouter le markup d'un formulaire simple permettant d'éditer un label et de le soumettre.
  ``` html
  <form>
    <input name="label" type="text" placeholder="Enter todo item name"/>
    <button type="submit">Create</button>
  </form>
  ```
* Créer une propriété `label`, de type `string` dans la classe `todo-form`
* Utiliser la directive `[(ngModel)]` sur le tag &lt;input&gt; et lui assigner la propriété `label` nouvellement créée
  * La console va produire une erreur car le module `FormsModule` d'Angular doit être importé dans le projet
  * Importer le module `FormsModule` dans le module `todo`
    ``` typescript
    import { FormsModule } from '@angular/forms';
    ```
  * Le rajouter au bloc `imports` du module
    ``` typescript
      imports: [
        CommonModule,
        FormsModule,
      ],
    ```
  * L'erreur devrait disparaître de la console

* Créer une méthode `submit` dans la classe `todo-form`.\
  Faire un `console.log` de la valeur de la propriété `label` lors de l'appel de la méthode
* Lier la méthode `submit` à l'événement éponyme `submit` du tag &lt;form&gt; dans la vue du formulaire.\
* Lorsque vous soumettez le formulaire en pressant enter ou en cliquant sur le bouton submit, vous devriez voir la valeur actuelle de `label` apparaître dans la console.

## 2. Créer et injecter un service `todo`

Ce type d'élément Angular sera abordé plus tard dans le cours.

* Copier les fichiers `todo.service.ts` et `todo-statuses.ts` de la solution dans le même répertoire que le module `todo.module.ts`
* Rajouter les imports ci-dessous au module `todo.module`
  ``` typescript
  import { LOCAL_STORAGE } from 'ngx-webstorage-service';
  import { TodoService, TODO_SERVICE_STORAGE } from './todo.service';
  ```

* Toujours dant le module `todo.module`, rajouter la propriété `providers` ci-dessous, à la suite du tableau `declarations`
  ``` typescript
    providers: [
      { provide: TODO_SERVICE_STORAGE, useExisting: LOCAL_STORAGE },
      TodoService,
    ],
  ```
* Nous verrons cela en détail, mais vous avez déclaré le service `todo.service` au sein du module `todo.module`. Vous pouvez désormais l'injecter dans différents fichiers. Nous allons le considérer pour le moment comme une boîte noire, l'intellisense de votre IDE devrait vous fournir les méthodes disponibles au sein du service.

## 3. Rajouter un état partagé

* Injecter le service `todo.service` dans la classe de votre composant `todo-list` de l'exercice précédent
  ``` typescript
  import { TodoService } from '../todo.service';
  ```
  ``` typescript
  constructor(private service: TodoService) {}
  ```

* Ce service va permettre de partager un état commun entre différents composants.
* Remplacer la propriété `items` de la classe `todo-list` par un getter du même nom.\
  Faire en sorte que le getter expose la propriété `items` du service.
* La liste d'éléments devrait maintenant être vide (c'est normal).

* Injecter le service `todo.service` dans la classe de votre nouveau composant `todo-form`
* Remplacer le corps de la méthode submit pour effectuer un appel à la méthode `addItem` du service.\
  Réinitialiser la valeur de la propriété `label` après avoir créé un nouvel élément
* Des éléments devraient apparaître dans la liste `todo-list` lorsque le formulaire est soumis