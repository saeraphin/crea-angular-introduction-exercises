# CREA Angular Introduction Exercises

These exercises goal is to create a local storage based todo list from scratch.

They will cover various basic Angular principles and aspects.

![Angular Todo list screenshot](00_todo-list-start/src/assets/images/todo.png?raw=true "Angular Todo list")

## Required setup

The exercises make active use of the [Angular CLI](https://github.com/angular/angular-cli).
Please run the following command to install it.

```
$ npm install -g @angular/cli
```

Go to the `00_todo-list-start` folder and run the install script.

```
$ cd 00_todo-list-start
$ npm install
```

You should then be able to launch a local dev server (notice we are using `ng` which corresponds to the Angular CLI).

```
$ ng serve
```

## Folder organisation

Each exercise consists in a series of tasks to be performed. The state you'll reach at the end of each exercise will serve as the starting point of the next exercise.

The changes you will be required to make should only affect the files in the `src/app` folder.

A `solution` folder is present in each exercise folder, it contains the final state of each exercise. It is recommended not to look at the content of these files straight away.

Should you not be happy with the current state of your work at the end of an exercise, you can back up your current work in a local Git branch and replace the content of the `00_todo-list-start/src/app` folder with the previous exercise `solution` folder.

The `99_todo-list-final` folder contains the final state of the exercise with a fully functional todo list. It is recommended to only consult this folder at the end of the day.