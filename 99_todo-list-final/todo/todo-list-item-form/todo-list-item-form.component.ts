import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-todo-list-item-form',
  templateUrl: './todo-list-item-form.component.html',
  styleUrls: ['./todo-list-item-form.component.css']
})
export class TodoListItemFormComponent implements OnInit {

  @Input() label:string = ''

  @Output() submitEvent = new EventEmitter<string>()
  @Output() cancelEvent = new EventEmitter<void>()

  form: FormGroup = new FormGroup({
    label: new FormControl('', [Validators.required, Validators.minLength(4)])
  })

  ngOnInit() {
    this.form.patchValue({ label: this.label })
  }

  submit() {
    const { label } = this.form.value
    this.submitEvent.emit(label)
  }

  cancel() {
    this.cancelEvent.emit()
  }

}
