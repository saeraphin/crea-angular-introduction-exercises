export enum TodoStatuses {
  All = 'ALL',
  Unfinished = 'UNFINISHED',
  Finished = 'FINISHED',
}
