# Angular - exercice 3

Cet exercice continuera à se faire dans le module `todo`.

## 1. Créer un composant d'élément de liste

![Angular Todo list item screenshot](../00_todo-list-start/src/assets/images/todo-list-item.png?raw=true "Angular Todo list - item")

* Créer un composant `todo-list-item`\
  *Exécuter la commande depuis le répertoire `00_todo-list-start`*
  ```
  ng generate component todo/todo-list-item
  ```

* Modifier le sélecteur du composant pour se baser sur un attribut, cela permet de décider du tag parent à utiliser.\
  Noter la présence de `label`, qui permet de forcer la présence des deux attributs pour être pris pour cible par le composant
  ```
    selector: '[app-todo-list-item]',
  ```

* Rajouter le composant `todo-list-item` dans la boucle de la vue du composant `todo-list`.\
  Pour ce faire, rajouter l'attribut spécifié en tant que sélecteur à l'étape précédente au tag &lt;li&gt; de la liste
* Ce nouveau composant va permettre de revoir la liste et de déplacer la vue des éléments de liste dans un composant dédié.
* Rajouter le markup des éléments de liste et rajouter un nouveau bouton de suppression
  ``` html
  <label>
    <input type="checkbox">
    someLabel
  </label>
  <button type="button">Delete</button>
  ```
* La liste devrait afficher les mêmes valeurs pour tous les éléments, il va falloir corriger cela

## 2. Communication parent > enfant

* Créer deux propriétés `label:string` et `done:boolean` dans la classe du composant `todo-list-item`.\
  Les marquer comme entrées avec le décorateur `@Input()`
* Rajouter la propriété `label` dans la vue de l'élément de liste
* Rajouter la propriété `done` en tant que `ngModel` sur le tag &lt;input&gt;
* Dans le composant parent `todo-list`, passer les deux nouvelles propriétés d'entrées aux éléments de la liste
* L'affiche dynamique de la liste devrait être restauré

## 3. Communication enfant > parent

### 3.1 Modification de l'enfant

* Créer deux propriétés de sortie dans la classe du composant `todo-list-item`
  * `@Output() toggleEvent = new EventEmitter<boolean>()`
  * `@Output() deleteEvent = new EventEmitter<void>()`
* Créer deux méthodes `toggle` et `delete` dans la classe du composant `todo-list-item`
* Lier la méthode `toggle` à l'événement `change` du tag &lt;input&gt;.\
  Émettre la valeur de la propriété `done` au travers de l'émetteur de sortie `toggleEvent`
* Lier la méthode `delete` au clic sur le bouton de suppression.\
  Faire émettre l'émetteur de sortie `deleteEvent` (aucune valeur requise)

### 3.2 Modification du parent

* Créer une méthode `onToggle` dans la classe du composant `todo-list`
* La méthode doit prendre en paramètre un élément `TodoItem` et un booléen
* Lier la méthode `onToggle` à l'événement de sortie `toggleEvent`
* `onToggle` doit appeler la méthode `updateItemStatus` du service `todo.service`

* Créer une méthode `onDelete` dans la classe du composant `todo-list`
* La méthode doit prendre en paramètre un élément `TodoItem`
* Lier la méthode `onDelete` à l'événement de sortie `deleteEvent`.
* `onDelete` doit appeler la méthode `deleteItem` du service `todo.service`

* Le status de complétion d'un élément `TodoItem` est maintenant stocké dans le service `todo.service`
* Il est maintenant possible de supprimer un élément de la liste