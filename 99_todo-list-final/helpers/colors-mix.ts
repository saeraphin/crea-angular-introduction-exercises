export const padStart = (text: string, length: number = 2) => {
  const zeros = new Array(length).join('0')
  return (zeros + text).slice(-length)
}

const d2h = (d:number) => d.toString(16)
const h2d = (h:string) => parseInt(h, 16)

const mix = (color_1:string, color_2:string, weight: number = 0.5) => {
  color_1 = color_1.replace('#', '')
  color_2 = color_2.replace('#', '')

  if (weight < 0) weight = 0
  if (weight > 1) weight = 1

  let result = '#'

  for (let i = 0; i <= 5; i += 2) {
    const part1 = h2d(color_1.substring(i, i + 2))
    const part2 = h2d(color_2.substring(i, i + 2))

    let mixed = d2h(Math.abs(Math.floor(part1 + (part2 - part1) * weight)))

    result += padStart(mixed)
  }

  return result
}

export default mix
