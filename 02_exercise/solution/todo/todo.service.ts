import { Inject, Injectable, InjectionToken } from '@angular/core';
import { StorageService } from 'ngx-webstorage-service';
import { v4 as uuidv4 } from 'uuid';
import { TodoItem } from './todo-item';
import { TodoStatuses } from './todo-statuses';

export const TODO_SERVICE_STORAGE =
    new InjectionToken<StorageService>('TODO_SERVICE_STORAGE')

const STORAGE_KEY = 'angularTodoList'

@Injectable({
  providedIn: 'any'
})
export class TodoService {
  private _items: Array<TodoItem> = []
  private _activeFilter: TodoStatuses = TodoStatuses.All

  constructor(
    @Inject(TODO_SERVICE_STORAGE) private storage: StorageService
  ) {
    this._items = (this.storage.get(STORAGE_KEY) || []) as TodoItem[]
  }

  updateStorage() {
    this.storage.set(STORAGE_KEY, this._items)
  }

  get items() {
    return this._items
  }

  get finishedItems() {
    return this._items.filter(item => item.done)
  }

  get unfinishedItems() {
    return this._items.filter(item => !item.done)
  }

  get filteredItems() {
    switch (this._activeFilter) {
      case TodoStatuses.Finished:
        return this.finishedItems
      case TodoStatuses.Unfinished:
        return this.unfinishedItems
      case TodoStatuses.All:
      default:
        return this.items
    }
  }

  get activeFilter() {
    return this._activeFilter
  }

  addItem(label: string) {
    this._items = [
      ...this._items,
      {
        label,
        done: false,
        id: uuidv4(),
      }
    ]

    this.updateStorage()
  }

  deleteItem(target: TodoItem) {
    if (!this._items.includes(target))
      return

    this._items = this._items.filter(item => item !== target)

    this.updateStorage()
  }

  updateItemStatus(target: TodoItem, done: boolean) {
    if (!this._items.includes(target) || target.done === done)
      return

    this._items = this._items.map(item => {
      if (item === target)
        return { ...item, done }
      return item
    })

    this.updateStorage()
  }

  updateItemLabel(target: TodoItem, label: string) {
    if (!this._items.includes(target) || target.label === label)
      return

    this._items = this._items.map(item => {
      if (item === target)
        return { ...item, label }
      return item
    })

    this.updateStorage()
  }

  updateFilter(value: TodoStatuses) {
    this._activeFilter = value
  }
}
