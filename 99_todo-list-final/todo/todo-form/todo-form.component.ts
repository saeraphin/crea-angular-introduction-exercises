import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.css']
})
export class TodoFormComponent {

  form: FormGroup = new FormGroup({
    label: new FormControl('', [Validators.required, Validators.minLength(4)])
  })

  constructor(private service: TodoService) {}

  submit() {
    const { label } = this.form.value
    this.service.addItem(label)
    this.form.reset()
  }

  cancel() {
    this.form.reset()
  }

}
