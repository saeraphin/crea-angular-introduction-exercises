# Angular - exercice 5

Cet exercice aura lieu dans les modules `shared` et `todo`.

## 1. Rajout d'un module partagé

Suivre les instructions du point 5 de l'exercice 4, ou repartir de la solution de l'exercice 4.

## 2. Création d'une directive autofocus

* Créer une directive `autofocus`\
  *Exécuter la commande depuis le répertoire `00_todo-list-start`*
  ```
  ng generate directive shared/autofocus
  ```

* Rajouter la nouvelle directive aux exports du module `shared` pour le rendre disponible en dehors du module
* Par défaut, les directives sont créées avec un sélecteur basé sur les attributs, cela conviendra pour cet exemple.\
  Une directive est inerte à sa création, car les possibilités d'application sont très variées

## 3. Fonction autofocus

* Le but de la directive et de déplacer le focus sur l'élément auquel elle est appliquée.\
  Pour ce faire, un pointeur vers l'élément est requis. En Angular, il faut pour cela injecter le pointeur dans le constructeur.
  ``` typescript
  import { /* ... */, ElementRef } from '@angular/core';
  ```
  ``` typescript
  constructor(private elementRef: ElementRef) { }
  ```

* **Attention, l'utilisation de `elementRef` peut exposer votre application à des attaques XSS. L'élément récupéré par cette méthode ne devrait pas être modifié directement dans votre code, car il ne sera pas soumis aux filtres de sécurité Angular.**\
  Récupérer l'élément et lire ses propriétés est sans danger, mais il faut éviter de le modifier directement sans passer par les méthodes d'assainissement d'Angular.
  ``` typescript
  // risque potentiel
  constructor(private elementRef: ElementRef) {
    this.elementRef.nativeElement.onclick = someUserDefinedFunction
  }
  ```

* La référence à l'élément n'est disponible qu'après l'étape `ngAfterViewInit` du cycle de vie d'Angular.\
  Il faut donc se brancher sur cette étape du cycle de vie pour effectuer notre action.
  ``` typescript
  import { AfterViewInit, /* ... */ } from '@angular/core';
  ```
  Pour se brancher sur un *hook* du cycle de vie, il faut que l'élément Angular implémente l'interface de la ou les étapes désirées
  ``` typescript
  export class AutofocusDirective implements AfterViewInit { /* ... */ }
  ```
  Une fois le mot-clé `implements` utilisé, l'IDE devrait afficher une erreur tant que la classe n'implémente pas effectivement les interfaces annoncées
  ``` typescript
  ngAfterViewInit() { /* ... */ }
  ```

* Dans l'étape `ngAfterViewInit`, rajouter le focus sur l'élément `elementRef` injecté dans la directive
  ``` typescript
  this.elementRef.nativeElement.focus()
  ```

## 4. Utiliser la directive

* Comme la directive a été ajoutée à la liste des exports du module `shared`, tout module qui importe le module `shared` pourra faire usage des éléments que ce dernier exporte (composants, directives, etc...)
* Comme le module `todo` importe le module `shared`, il peut donc utiliser la nouvelle directive `autofocus`
* Rajouter l'attribut `appAutofocus` sur le tag &lt;input&gt; dans la vue du composant `todo-list-item-form`.\
  *Cet attribut est le sélecteur qui est déclaré dans la classe de la directive. Avec l'usage que nous en faisons, sa présence sans valeur suffit à appliquer la fonctionnalité de la directive.\
  On parle alors d'un attribut **unaire** (états possibles: présent ou absent)*
* Lors de l'affichage du formulaire d'édition du label, le tag &lt;input&gt; devrait maintenant recevoir le focus automatiquement