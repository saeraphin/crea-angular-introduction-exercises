import { Component } from '@angular/core';
import { TodoStatuses } from '../todo-statuses';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-todo-filters',
  templateUrl: './todo-filters.component.html',
  styleUrls: ['./todo-filters.component.css'],
  host: { class: 'field has-addons buttons are-small' }
})
export class TodoFiltersComponent {

  statuses = TodoStatuses

  constructor(private service: TodoService) { }

  asStatus(value: string) {
    return TodoStatuses[value as keyof typeof TodoStatuses]
  }

  isStatusSelected(value: string) {
    const status = this.asStatus(value)
    return status === this.service.activeFilter
  }

  select(value: string) {
    const status = this.asStatus(value)
    this.service.updateFilter(status)
  }

}
