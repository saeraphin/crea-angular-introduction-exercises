import { Component } from '@angular/core';
import { TodoItem } from '../todo-item';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
})
export class TodoListComponent {

  constructor (private service: TodoService) {}

  get items() {
    return this.service.items
  }

  trackBy(index: number, item: TodoItem) {
    return item.id
  }

  onToggle(item: TodoItem, done: boolean) {
    this.service.updateItemStatus(item, done)
  }

  onDelete(item: TodoItem) {
    this.service.deleteItem(item)
  }
}
