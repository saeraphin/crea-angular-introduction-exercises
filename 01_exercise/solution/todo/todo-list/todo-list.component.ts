import { Component } from '@angular/core';
import { TodoItem } from '../todo-item';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
})
export class TodoListComponent {

  items: TodoItem[] = [
    {id: "1", label: 'Sleep earlier', done: false},
    {id: "2", label: 'Learn JavaScript', done: false},
    {id: "3", label: 'Master Angular', done: false},
    {id: "4", label: 'Enjoy Coding', done: false},
    {id: "5", label: 'Understand Git', done: false},
  ]

  trackBy(index: number, item: TodoItem) {
    return item.id
  }
}
