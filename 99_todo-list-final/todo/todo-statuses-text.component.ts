import { Component, Input } from '@angular/core';
import { TodoStatuses } from './todo-statuses';

@Component({
  selector: 'app-todo-statuses-text',
  template: `
  <ng-container [ngSwitch]="key">
    <ng-container *ngSwitchCase="statuses.All">All</ng-container>
    <ng-container *ngSwitchCase="statuses.Unfinished">Yet to be done</ng-container>
    <ng-container *ngSwitchCase="statuses.Finished">Done</ng-container>
    <ng-container *ngSwitchDefault>undefined</ng-container>
  </ng-container>
  `,
})
export class TodoStatusesTextComponent {

  @Input() key!: TodoStatuses

  statuses = TodoStatuses

}
