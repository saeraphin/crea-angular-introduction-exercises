# Angular - exercice 6

Cet exercice aura lieu dans les modules `shared` et `todo`.

## 1. Création d'un composant container

* Créer un composant `box-with-title`\
  *Exécuter la commande depuis le répertoire `00_todo-list-start`*
  ```
  ng generate component shared/box-with-title
  ```

* Rajouter le composant `box-with-title` la vue du composant `todo`.\
  (`src\app\todo\todo\todo.component.html`)
* Ce nouveau composant va consister en un container avec une titre *(quelle surprise)*.\
  Dans le contexte d'une application plus vaste, cela permettrait de centraliser la vue et le style d'un container réutilisable sans avoir à répéter le même code plusieurs fois *(deux fois, c'est une fois de trop)*
* Créer le markup du composant, pour ce faire, extraire le container actuel du composant `todo` et son titre et les déplacer dans le nouveau composant que nous venons de créer
* Copier-coller le style du composant `todo` dans la feuille de style du composant `box-with-title`
* Supprimer la feuille de style `todo.component.css` et sa référence dans la classe `todo`

* Le contenu du composant devrait maintenant (vaguement) apparaître, mais le but recherché n'est pas encore atteint. Ce n'est qu'une coquille vide

## 2. Implémenter la projection de contenu

* L'utilisation du tag &lt;ng-content&gt; va permettre de déclarer où va s'effectuer le rendu des enfants du container
* Rajouter un premier tag &lt;ng-content&gt; à la suite du titre dans la vue du composant `box-with-title`
* Dans la vue du composant `todo`, déplacer les enfants du composant dans le composant `box-with-title`
  ``` html
  <app-box-with-title>
    <h2>
      <em>Angular</em> Todo list
    </h2>
    <app-todo-form></app-todo-form>
    <div class="notification is-info is-light">Filters</div>
    <div class="notification is-info is-light">Counter</div>
    <app-todo-list></app-todo-list>
  </app-box-with-title>
  ```

* Les enfants du composant `todo` devraient maintenant être rendus dans le composant `box-with-title`
* Il reste à déclarer le slot destiné à contenir le titre
* Rajouter un titre &lt;h2&gt; dans la vue du composant `box-with-title`.\
* Rajouter un tag &lt;ng-content&gt; dans le titre de la vue du composant `box-with-title`.\
  Cette fois, rajouter un selecteur basé sur l'attribut `box-title`
* Rajouter un tag &lt;ng-container&gt; avec un attribut `box-title` à la vue du composant `todo`.\
  Y déplacer le contenu du tag &lt;h2&gt;

* Le contenu du tag &lt;ng-content box-title&gt; devrait maintenant être projeté dans le container &lt;ng-content&gt; lié à cet attribut

## 3. Extra

La solution aux exercices suivants figure dans le répertoire `99_todo-list-final`. L'idée est ici d'essayer d'implémenter deux derniers composants en se basant sur les notions acquises dans la série d'exercices.

### 3.1 Compteur

![Angular Todo list counter screenshot](../00_todo-list-start/src/assets/images/todo-counter.png?raw=true "Angular Todo list - counter")

* Créer et implémenter le composant `todo-counter`
* Ce composant doit se baser sur l'état du service `todo.service` (il faudra donc l'y injecter)
* Le service `todo.service` expose les tableaux `items` et `finishedItems`, utiliser les longueurs de ces tableaux pour alimenter le composant
* La classe du composant dans la solution comporte une logique supplémentaire pour faire varier la couleur de la pastille de gauche, vous pouvez y jeter un oeil pour avoir une idée du genre d'amélioration *nice-to-have* que vous pouvez rajouter à des composants quand vous en avez le temps

### 3.2 Filtres

![Angular Todo list filters screenshot](../00_todo-list-start/src/assets/images/todo-filters.png?raw=true "Angular Todo list - filters")

* Créer et implémenter le composant `todo-filters`
* Ce composant doit se baser sur l'état du service `todo.service` (il faudra donc l'y injecter)
* Le service `todo.service` expose le tableau `filteredItems`, utiliser celui-ci à la place du tableau items dans le composant `todo-list`
* Le service `todo.service` expose la propriété `activeFilter` et la méthode `updateFilter`, utiliser ces deux éléments pour implémenter la logique du composant.\
  Lors de l'utilisation de la méthode `updateFilter`, le tableau retourné par `filteredItems` var être mis à jour en fonction du filtre passé en paramètre
* La classe du composant `todo-list` dans la solution comporte une logique supplémentaire pour afficher différents messages si aucune tâche n'est retournée par le filtre sélectionné, vous pouvez y jeter un oeil pour avoir une idée du genre d'amélioration *nice-to-have* que vous pouvez rajouter à des composants quand vous en avez le temps