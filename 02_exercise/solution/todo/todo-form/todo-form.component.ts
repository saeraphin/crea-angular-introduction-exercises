import { Component } from '@angular/core';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.css']
})
export class TodoFormComponent {

  label = ''

  constructor(private service: TodoService) {}

  submit() {
    this.service.addItem(this.label)
    this.label = ''
  }

  cancel() {
    this.label = ''
  }

}
