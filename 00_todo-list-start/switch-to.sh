#!/bin/bash

backup_files()
{
    # get current date
    now=$(date +%H%M%S)

    echo "Backing up the current working directory to a dedicated branch."
    git checkout -b "backup_$now"
    git add --all
    git commit -a -m "Backed up exercise files at $now status before switching to desired exercise $1."
    git checkout master
}

if [[ "$1" =~ ^[1-6]{1}$ ]]; then
    num="0$1"
    echo "Switching to exercise $num..."
    backup_files $num
    echo "Copying the contents of exercise $num folder."
    cp -a ../${num}_exercise/solution/. src/app
    echo "Your working directory is now at $num."

elif [ "$1" = "0" -o "$1" = "start" ]; then
    num="00"
    echo "Switching to exercise $num..."
    backup_files $num
    echo "Reseting files to master and cleaning untracked files."
    git reset --hard origin master
    git clean -fd
    echo "Your working directory is now back at its start state."

elif [ "$1" = "9" -o "$1" = "99" -o "$1" = "final" ]; then
    num="99"
    echo "Switching to final state."
    backup_files $num
    echo "Copying the contents of the final state folder."
    cp -a ../99_todo-list-final/. src/app
    echo "Your working directory is now at its final state."

else
    echo "The given parameter "$1" should be a number from 1 to 6, or 0, start, 9, final"
fi
