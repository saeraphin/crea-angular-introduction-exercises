import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-todo-list-item-form',
  templateUrl: './todo-list-item-form.component.html',
  styleUrls: ['./todo-list-item-form.component.css']
})
export class TodoListItemFormComponent {

  @Input() label:string = ''

  @Output() submitEvent = new EventEmitter<string>()
  @Output() cancelEvent = new EventEmitter<void>()

  submit() {
    this.submitEvent.emit(this.label)
  }

  cancel() {
    this.cancelEvent.emit()
  }

}
