import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: '[app-todo-list-item][label]',
  templateUrl: './todo-list-item.component.html',
  styleUrls: ['./todo-list-item.component.css']
})
export class TodoListItemComponent {

  @Input() label:string = ''
  @Input() done:boolean = false

  isEditing = false

  @Output() toggleEvent = new EventEmitter<boolean>()
  @Output() editedEvent = new EventEmitter<string>()
  @Output() deleteEvent = new EventEmitter<void>()

  toggle() {
    this.toggleEvent.emit(this.done)
  }

  updateLabel(label: string) {
    this.editedEvent.emit(label)
    this.toggleEditing(false)
  }

  delete() {
    this.deleteEvent.emit()
  }

  toggleEditing(value = true) {
    this.isEditing = value
  }

}
