import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: '[app-todo-list-item][label]',
  templateUrl: './todo-list-item.component.html',
  styleUrls: ['./todo-list-item.component.css']
})
export class TodoListItemComponent {

  @Input() label:string = ''
  @Input() done:boolean = false

  @Output() toggleEvent = new EventEmitter<boolean>()
  @Output() deleteEvent = new EventEmitter<void>()

  toggle() {
    this.toggleEvent.emit(this.done)
  }

  delete() {
    this.deleteEvent.emit()
  }

}
