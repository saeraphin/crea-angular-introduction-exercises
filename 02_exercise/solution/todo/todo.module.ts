import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LOCAL_STORAGE } from 'ngx-webstorage-service';
import { TodoService, TODO_SERVICE_STORAGE } from './todo.service';
import { TodoComponent } from './todo/todo.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoFormComponent } from './todo-form/todo-form.component';

@NgModule({
  declarations: [
    TodoComponent,
    TodoListComponent,
    TodoFormComponent,
  ],
  providers: [
    { provide: TODO_SERVICE_STORAGE, useExisting: LOCAL_STORAGE },
    TodoService,
  ],
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports: [
    TodoComponent,
  ]
})
export class TodoModule { }
