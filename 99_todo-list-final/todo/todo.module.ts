import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LOCAL_STORAGE } from 'ngx-webstorage-service';
import { SharedModule } from '../shared/shared.module';
import { TodoService, TODO_SERVICE_STORAGE } from './todo.service';
import { TodoComponent } from './todo/todo.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoListItemComponent } from './todo-list-item/todo-list-item.component';
import { TodoListItemFormComponent } from './todo-list-item-form/todo-list-item-form.component';
import { TodoCounterComponent } from './todo-counter/todo-counter.component';
import { TodoFiltersComponent } from './todo-filters/todo-filters.component';
import { TodoStatusesTextComponent } from './todo-statuses-text.component';
import { TodoFormComponent } from './todo-form/todo-form.component';

@NgModule({
  declarations: [
    TodoComponent,
    TodoListComponent,
    TodoListItemComponent,
    TodoListItemFormComponent,
    TodoCounterComponent,
    TodoFiltersComponent,
    TodoStatusesTextComponent,
    TodoFormComponent,
  ],
  providers: [
    { provide: TODO_SERVICE_STORAGE, useExisting: LOCAL_STORAGE },
    TodoService,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  exports: [
    TodoComponent,
  ]
})
export class TodoModule { }
