import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'enumToArray'
})
export class EnumToArrayPipe implements PipeTransform {

  transform<T>(value: Object): Array<string> {
    return Object.keys(value);
  }

}
