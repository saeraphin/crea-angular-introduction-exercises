import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MatIconRegistry, MatIconModule } from '@angular/material/icon';
import { BoxWithTitleComponent } from './box-with-title/box-with-title.component';
import { AutofocusDirective } from './autofocus.directive';
import { DomSanitizer } from '@angular/platform-browser';
import { EnumToArrayPipe } from './enum-to-array.pipe';

@NgModule({
  declarations: [
    BoxWithTitleComponent,
    AutofocusDirective,
    EnumToArrayPipe,
  ],
  imports: [
    CommonModule,
    // Required by the Angular Material icon module
    HttpClientModule,
    MatIconModule,
  ],
  exports: [
    BoxWithTitleComponent,
    AutofocusDirective,
    EnumToArrayPipe,
    MatIconModule,
  ]
})
export class SharedModule {
  constructor (
    matIconRegistry: MatIconRegistry,
    domSanitizer: DomSanitizer,
  ) {
    matIconRegistry.addSvgIconSet(
      domSanitizer.bypassSecurityTrustResourceUrl('./assets/mdi.svg')
    )
  }
}
