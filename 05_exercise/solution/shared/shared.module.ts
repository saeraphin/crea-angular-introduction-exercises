import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MatIconRegistry, MatIconModule } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { EnumToArrayPipe } from './enum-to-array.pipe';
import { AutofocusDirective } from './autofocus.directive';

@NgModule({
  declarations: [
    EnumToArrayPipe,
    AutofocusDirective,
  ],
  imports: [
    CommonModule,
    // Required by the Angular Material icon module
    HttpClientModule,
    MatIconModule,
  ],
  exports: [
    EnumToArrayPipe,
    MatIconModule,
    AutofocusDirective,
  ]
})
export class SharedModule {
  constructor (
    matIconRegistry: MatIconRegistry,
    domSanitizer: DomSanitizer,
  ) {
    matIconRegistry.addSvgIconSet(
      domSanitizer.bypassSecurityTrustResourceUrl('./assets/mdi.svg')
    )
  }
}
