import { Component } from '@angular/core';
import { TodoItem } from '../todo-item';
import { TodoStatuses } from '../todo-statuses';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
})
export class TodoListComponent {

  statuses = TodoStatuses

  constructor(private service: TodoService) { }

  get items() {
    return this.service.filteredItems
  }

  get isEmpty() {
    return this.service.items.length === 0
  }

  get isActiveEmpty() {
    return !this.isEmpty && this.items.length === 0
  }

  get isAllDone() {
    return this.isActiveEmpty && this.service.activeFilter === TodoStatuses.Unfinished
  }

  get isNoneDone() {
    return this.isActiveEmpty && this.service.activeFilter === TodoStatuses.Finished
  }

  trackBy(index: number, item: TodoItem) {
    return item.id
  }

  onToggle(item: TodoItem, done: boolean) {
    this.service.updateItemStatus(item, done)
  }

  onEdited(item: TodoItem, label: string) {
    this.service.updateItemLabel(item, label)
  }

  onDelete(item: TodoItem) {
    this.service.deleteItem(item)
  }
}
