# Angular - exercice 1

La majorité des exercices se fera dans le module `todo`.
Nous créerons un module `shared` par la suite.

Pour créer un élément (comme un composant) dans un module, utiliser le chemin du module `moduleName/elementName`.

## 1. Créer un composant liste

![Angular Todo list screenshot](../00_todo-list-start/src/assets/images/todo-list.png?raw=true "Angular Todo list - list")

* Créer un composant `todo-list`\
  *Exécuter la commande depuis le répertoire `00_todo-list-start`*
  ```
  ng generate component todo/todo-list
  ```
* Rajouter le composant `todo-list` dans la vue du composant `todo` (Remplacer le bloc `List`)\
  `00_todo-list-start\src\app\todo\todo\todo.component.html`
* Rajouter un tableau de *todos* dans la classe `todo-list`

  ``` typescript
  items: any[] = [
    {id: "1", label: 'Sleep earlier', done: false},
    {id: "2", label: 'Learn JavaScript', done: false},
    {id: "3", label: 'Master Angular', done: false},
    {id: "4", label: 'Enjoy Coding', done: false},
    {id: "5", label: 'Understand Git', done: false},
  ]
  ```
* Créer une interface `todo-item` dans le module `todo`\
  *Noter qu'une interface ne peut pas appartenir à un module, il suffit alors de passer le répertoire en paramètre*
  ```
  ng generate interface todo/todo-item
  ```

* Rajouter les propriétés des éléments du tableau à l'interface
  ``` typescript
  export interface TodoItem {
    id: string;
    label: string;
    done: boolean;
  }
  ```

* Remplacer le type `any` par `TodoItem` dans la classe `todo-list`

## 2. Lister les tâches

* Supprimer le texte par défaut "todo-list works!"
* Créer une liste non ordonnée &lt;ul&gt; de *todos* en utilisant la directive ngFor

  ``` html
  <li>
    <label>
      <input type="checkbox">
      todo name
    </label>
  </li>
  ```

* Lier le texte affiché dans le &lt;label&gt; à la propriété `label` des éléments *todo*
* Lier l'attribut checked des &lt;input&gt; à la propriété `done` des éléments *todo*
* Rajouter une méthode d'identification `trackBy` à la classe `todo-list`
* Rajouter la méthode à la directive ngFor de la vue `todo-list`
