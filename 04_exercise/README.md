# Angular - exercice 4

Cet exercice continuera à se faire dans le module `todo`.
Cependant, le nouveau module `shared` sera copié à la fin de l'exercice.

## 1. Créer un formulaire d'édition d'élément de liste

![Angular Todo list item form screenshot](../00_todo-list-start/src/assets/images/todo-list-item-form.png?raw=true "Angular Todo list - item form")

* Créer un composant `todo-list-item-form`\
  *Exécuter la commande depuis le répertoire `00_todo-list-start`*
  ```
  ng generate component todo/todo-list-item-form
  ```

* Rajouter le composant `todo-list-item-form` la vue du composant `todo-list-item`
* Ce nouveau composant va permettre d'éditer le label d'un élément TodoItem existant
* Rajouter le markup d'un formulaire avec un champ texte, un bouton annuler et un bouton de soumission
  ``` html
  <form>
    <input name="label" type="text">
    <button type="button">Cancel</button>
    <button type="submit">Confirm</button>
  </form>
  ```
* Le formulaire devrait apparaître dans chaque élément de la liste, il est cependant inerte et encombrant

## 2. Communication parent > enfant

* Créer une propriété `label:string` dans la classe du composant `todo-list-item-form`.\
  La marquer comme entrée avec le décorateur `@Input()`
* Rajouter la propriété `label` en tant que `ngModel` sur le tag &lt;input&gt;
* Dans le composant parent `todo-list-item`, passer la nouvelle propriété d'entrée aux formulaires de la liste

## 3. Communication enfant > parent

### 3.1 Modification de l'enfant

* Créer deux propriétés de sortie dans la classe du composant `todo-list-item-form`
  * `@Output() submitEvent = new EventEmitter<string>()`
  * `@Output() cancelEvent = new EventEmitter<void>()`
* Créer deux méthodes `submit` et `cancel` dans la classe du composant `todo-list-item-form`
* Lier la méthode `submit` à l'événement `submit` du tag &lt;form&gt;.\
  Émettre la valeur de la propriété `label` au travers de l'émetteur de sortie `submitEvent`
* Lier la méthode `cancel` au clic sur le bouton d'annulation.\
  Faire émettre l'émetteur de sortie `cancelEvent` (aucune valeur requise)

### 3.2 Modification du parent

* Créer une nouvelle propriété de sortie dans la classe du composant `todo-list-item`
  * `@Output() editedEvent = new EventEmitter<string>()`
* Créer une méthode `updateLabel` dans la classe du composant `todo-list-item`
* La méthode doit prendre en paramètre un string
* Lier la méthode `updateLabel` à l'événement de sortie `submitEvent`.\
  Émettre la valeur reçue de l'enfant au travers de l'émetteur de sortie `editedEvent`.\
  Le composant `todo-list-item` ne fait ici que remonter l'événement d'édition à son parent

* Créer une propriété `isEditing = false` dans la classe du composant `todo-list-item`
* Créer une méthode `toggleEditing` dans la classe du composant `todo-list-item`
* La méthode doit prendre en paramètre un booléen
* Lier la méthode `toggleEditing` à l'événement de sortie `cancelEvent`.\
  `toggleEditing` doit rendre `isEditing` fausse lors d'un `cancelEvent`

### 3.3 Modification du parent du parent

* Créer une méthode `onEdited` dans la classe du composant `todo-list`
* La méthode doit prendre en paramètre un élément `TodoItem` et un string
* Lier la méthode `onEdited` à l'événement de sortie `editedEvent`
* `onEdited` doit appeler la méthode `updateItemLabel` du service `todo.service`

## 4. Affichage conditionnel du formulaire

* Modifier la vue du composant `todo-list-item`
* Mettre la vue par défaut du composant dans un &lt;ng-container&gt;
* Mettre l'instance du nouveau formulaire `todo-list-item-form` dans un &lt;ng-template&gt;
* Rendre l'affichage de ces deux nouveaux containers conditionnel avec la directive `*ngIf`
* Rajouter un bouton d'édition à la vue par défaut.\
  Lier le clic du bouton à la méthode `toggleEditing` utilisée précédemment.\
  Cette fois, passer la valeur `true` à la méthode pour activer l'édition.\
  C'est l'événement `cancelEvent` déjà en place de l'enfant qui permettra de sortir du mode d'édition

## 5. Rajout d'un module partagé

* Copier le répertoire `solution/shared` dans le répertoire `app`.\
  Il devrait se trouver au même niveau que le répertoire `todo`:\
  `app/todo` et `app/shared`
* Importer le nouveau module dans le module `todo.module`
  ``` typescript
  import { SharedModule } from '../shared/shared.module';
  ```
  Et le rajouter à la suite des `imports` déjà présents
  ``` typescript
    imports: [
      CommonModule,
      FormsModule,
      SharedModule,
    ],
  ```
* Ce module importe, configure et expose un module d'icônes
* Déplacer les textes de certains boutons dans leur balise title.\
  Les remplacer par des icônes, comme ceci:
  ``` html
  <mat-icon class="icon" svgIcon="pencil"></mat-icon>
  ```
* Référence pour les noms des icônes:\
  https://pictogrammers.github.io/@mdi/font/6.5.95/